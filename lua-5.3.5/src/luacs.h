#ifndef luacs_h
#define luacs_h
/**
* 调用该方法可以产生lua字节码
**/

int luac(int argc, char* argv[], unsigned char **outstr_p, unsigned int *outlen_p);
void luac_free();

#endif
