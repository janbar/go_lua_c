#ifndef common_h
#define common_h


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
  char  *data;
  size_t size;
}file_info;

// 将文件全部读到内存中,返回文件内容和文件长度
file_info *read_file(const char *file);

// 计算数据的crc32值
unsigned int crc32(const char *buf, unsigned int size);

#endif
