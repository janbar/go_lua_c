
set LUASRC=..\..\lua-5.3.5\src

if "%1%"=="c_call_lua" goto c_call_lua
if "%1%"=="lua_call_c" goto lua_call_c
if "%1%"=="test_luac" goto test_luac
goto eof

:c_call_lua
gcc c_call_lua.c -o c_call_lua.exe -I%LUASRC% -L%LUASRC% -llua
.\c_call_lua.exe 2 3
goto eof

:lua_call_c
gcc -Wall -shared -fPIC -o mylib.dll mylib.c -I%LUASRC% %LUASRC%\lua53.dll
%LUASRC%\lua lua_call_c.lua
goto eof

:test_luac
gcc test_luac.c -o test_luac.exe -I%LUASRC% -L%LUASRC% -llua
.\test_luac.exe
%LUASRC%\luac -p luac.out
%LUASRC%\lua luac.out
goto eof

:eof
