#include <stdio.h>
#include <stdlib.h>
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
lua_State* L = NULL;
/* 内部调用lua函数 */
double f( double x, double y )
{
    double z;
    lua_getglobal( L, "f" );
    /* 获取lua函数f */
    lua_pushnumber( L, x );
    /* 压入参数x和y */
    lua_pushnumber( L, y );
    if ( lua_pcall( L, 2, 1, 0 ) != 0 )
    {
        luaL_error( L, "error running function 'f': %s", lua_tostring( L, -1 ) );
        exit( 0 );
    }
    if ( !lua_isnumber( L, -1 ) )
    {
        luaL_error( L, "function 'f' must return a number" );
        exit( 0 );
    }
    z = lua_tonumber( L, -1 );
    lua_pop( L, 1 );
    return z;
}

int main( int argc, char *argv[] )
{
    double a,b;
    printf("input a:");
    scanf("%lf", &a);
    printf("input b:");
    scanf("%lf", &b);
    if ( NULL == (L = luaL_newstate()) )
    {
        return 0;
    }
    luaL_openlibs( L );
    if ( luaL_loadfile( L, "c_call_lua.lua" ) || lua_pcall( L, 0, 0, 0 ) )
    {
        luaL_error( L, "cannot run configuration file: %s\n", lua_tostring( L, -1 ) );
    }
    printf( "math.pow(a, b)=%f\n", f( a, b) );
    lua_close( L );
    return 0;
}
