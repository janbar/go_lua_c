#include <stdio.h>
#include <stdlib.h>

#include "luacs.h"
#include "common.h"

int main (int argc, char *argv[]) {
  const char *in_file  = "luac.lua";
  const char *out_file = "luac.out";
  
  file_info *f_info = read_file(in_file);
  if (NULL == f_info)
    return -1;
  char *str[] = {f_info->data};
  unsigned int outlen;
  unsigned char *outstr;
  int result = luac(1, str, &outstr, &outlen);
  if (result != 0) {
    printf("Error : %s", outstr);
    luac_free();
    return -1;
  }

  FILE *f = fopen(out_file, "wb");
  if (NULL == f) {
    printf("Open file failed!\n");
    luac_free();
    return -1;
  }
  if (fwrite(outstr, outlen, 1, f) != 1)
    printf("Write file failed!\n");
  if (fclose(f) != 0)
    printf("Close file failed!\n");
  luac_free();
  return 0;
}
