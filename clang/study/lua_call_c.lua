lib = require "mylib"

io.write("input a:")
a = io.read("*number")
io.write("input b:")
b = io.read("*number")
print("a+b=",lib.add(a, b))

--[[
mylib.c的命名和这里的require "mylib"保持一致
且mylib.c中的luaopen_xxx也要命名为luaopen_mylib
-- ]]