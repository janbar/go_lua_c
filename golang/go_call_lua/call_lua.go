package main

import (
	"log"

	"./lua"
)

/**
* 可以参考这个项目的经验
* https://github.com/aarzilli/golua
* https://www.lua.org/ftp/,下载5.1.5版本的lua
* 使用mingw32-make.exe mingw编译好
* 已经将对应.h文件和.a文件放到lua路径,直接可以使用
**/

func main() {
	L := lua.NewState()
	defer L.Close()
	L.OpenLibs()

	err := L.DoFile("lua_func.lua")
	if err != nil {
		log.Fatal(err)
	}

	L.GetGlobal("funcs")
	if L.IsTable(-1) {
		lt := L.ObjLen(-1)
		for i := 1; i <= lt; i++ {
			L.RawGeti(-1, i)
			if L.IsFunction(-1) {
				L.PushInteger(int64(i))
				L.PushString("sdfasdf")
				L.Call(2, 1)
				if L.IsString(-1) {
					log.Println(L.ToString(-1))
					L.Pop(1)
				}
			}
		}
	}

	L.GetGlobal("data")
	if L.IsTable(-1) {
		L.PushString("a")
		L.GetTable(-2)
		if L.IsNumber(-1) {
			log.Println("a", L.ToNumber(-1))
			L.Pop(1)
		}

		L.PushString("f")
		L.GetTable(-2)
		if L.IsFunction(-1) {
			L.PushString("func ...")
			L.Call(1, 0)
			L.Pop(1)
		}
	}
}
