function f1(i, data)
    print(data)
    return data .. i
end

function f2(i, data)
    print(data)
    return data .. i
end

function f3(i, data)
    print(data)
    return data .. i
end

funcs = {
    f1, f2, f3
}

function funcf(str)
    print(str)
end

data = {
    a = 123,
    f = funcf
}