package main

import (
	"io/ioutil"
	"log"
	"net"
)

func main() {
	ser, err := net.Listen("tcp", ":3456")
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Accept ok...")
	for {
		cli, err := ser.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go handleConn(cli)
	}
}

func handleConn(c net.Conn) {
	defer c.Close()

	var (
		buf = make([]byte, 2048)
		byt []byte
		err error
		num int
	)
	for {
		byt, err = ioutil.ReadFile("send_lua_code.lua")
		if err != nil {
			log.Println(err)
			return
		}
		byt, err = luac(byt)
		if err != nil {
			log.Println(err)
			return
		}
		c.Write(byt)

		num, err = c.Read(buf)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("%s\n", buf[:num])
	}
}
